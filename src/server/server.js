const express = require('express');

require('../config/globals');

const middleware = require('../config/middleware');
const mainRouter = require('../config/mainRouter');
const errorHandlers = require('../config/errorHandlers');

/**
 * @type {express}
 * @constant {express.Application}
 */
const app = express();

/**
 * @description express.Application Middleware
 */
middleware(app);

/**
 * @description express.Application Routes
 */
mainRouter(app);

/**
 * @description express.Application Errors
 */
errorHandlers(app);

/**
 * @description sets port 3000 to default or unless otherwise specified in the environment
 */
app.set('port', process.env.PORT || 3000);

module.exports = app;
