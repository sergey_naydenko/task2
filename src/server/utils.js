const { createTerminus } = require('@godaddy/terminus');
const mongoose = require('mongoose');

module.exports = (server) => {
    function onShutdown() {
        console.log('Preparing to shutdown');
        mongoose.connection.close(false, () => {
            console.log('MongoDb connection closed.');
        });
        console.log('Cleanup finished, server is shutting down');
    }

    const options = {
        signal: 'SIGTERM',
        onShutdown, // [optional] called right before exiting
    };

    createTerminus(server, options);
};
