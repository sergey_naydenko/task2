const { CronJob } = require('cron');

const job = new CronJob('0 * * * * *', () => {
    const memoryUsage = process.memoryUsage();

    // eslint-disable-next-line arrow-body-style
    const data = Object.keys(memoryUsage).map((key) => {
        // eslint-disable-next-line security/detect-object-injection
        return `${key}: ${Math.floor(memoryUsage[key] / 1024 / 1024)} Mb`;
    });

    console.log(JSON.stringify(data, null, 2));
}, null, false, 'America/Los_Angeles');

job.start();
