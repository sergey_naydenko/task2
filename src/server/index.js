const http = require('http');
const events = require('./events');
const server = require('./server');
const utils = require('./utils');
require('./cron');

const port = server.get('port');

const srv = http.createServer(server).listen(port);

events.bind(srv, port);

utils(srv);
