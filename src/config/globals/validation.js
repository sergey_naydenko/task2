const Joi = require('joi');
const { Types } = require('mongoose');

/**
 * @exports
 * @class Validation
 */
class Validation {
    /**
     * Creates an instance of Schema.
     * @constructor
     * @memberof JoiSchema
     */
    constructor() {
        /**
         * @static
         * @type {string}
         * @memberof JoiSchema
         */
        this.messageObjectId = 'Argument passed in must be a single String of 12 bytes or a string of 24 hex characters';
        this.Joi = Joi.extend({
            type: 'objectId',
            messages: {
                'objectId.base': this.messageObjectId,
            },
            validate(value, helpers) {
                return {
                    value,
                    ...!Types.ObjectId.isValid(value) && { errors: helpers.error('objectId.base') },
                };
            },
        });
    }
}

module.exports = Validation;
