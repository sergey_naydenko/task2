const baseUrlLocal = `http://localhost:${process.env.PORT || 3000}/`;
const baseUrlForEnv = {
    production: 'https://interns-blog.herokuapp.com/'
};

module.exports = baseUrlForEnv[process.env.NODE_ENV] || baseUrlLocal;
