/* eslint-disable global-require */
const APP = {
    // global npm modules
    moment: require('moment'),

    // global custom modules
    dbs: require('../dbs'),
    Validation: require('./validation'),
    ValidationError: require('../../error/ValidationError'),

    // global params
    baseUrl: require('./baseUrl'),

    // global functions
    asyncWrapper: fn => (req, res, next) => Promise.resolve(fn(req, res, next)).catch(next),
};

// Add APP namespace to globals
if (globalThis.APP) {
    throw new Error('Global var APP already in use');
} else {
    globalThis.APP = APP;
}
