const http = require('http');

const TopicRouter = require('../components/Topic/router');
const MessageRouter = require('../components/Message/router');
const AuthorizationRouter = require('../components/Authorization/router');

/**
 * @function
 * @param {express.Application} app
 * @summary init Application router
 * @returns void
 */
module.exports = (app) => {
    // load components routers
    app.use('/v1/auth', AuthorizationRouter);
    app.use('/v1/topic/:topicId/message', MessageRouter);
    app.use('/v1/topic', TopicRouter);
    app.get('/version', (req, res) => {
        res.send(`Application version : ${process.env.npm_package_version}`);
    });
    app.get('/', (req, res) => {
        res.redirect('/v1/topic');
    });
    app.get('/privacy', (req, res) => {
        res.render('privacy');
    });

    /**
     * @description No results returned mean the object is not found
     * @function
     * @inner
     * @param {callback} middleware - Express middleware.
     */
    app.use((req, res) => {
        res.status(404).send(http.STATUS_CODES[404]);
    });
};
