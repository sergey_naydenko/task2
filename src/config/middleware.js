const bodyParser = require('body-parser');
const express = require('express');
const path = require('path');
const compression = require('compression');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const helmet = require('helmet');
const passport = require('passport');
const redis = require('redis');
const session = require('express-session');
const RedisStore = require('connect-redis')(session);
require('dotenv').config();

const redisClient = redis.createClient({
    ...process.env.REDIS_URL && { url: process.env.REDIS_URL }
});

module.exports = (app) => {
    // security headers
    app.use(helmet({
        contentSecurityPolicy: false,
    }));

    // template engine
    app.set('view engine', 'pug');

    // pug - don't minify html
    app.locals.pretty = true;

    // views directory location
    app.set('views', './src/views');

    // static files location
    app.use(express.static(path.join(__dirname, '/../public')));

    // body parsers
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    // parse Cookie header and populate req.cookies with an object keyed by the cookie names.
    app.use(cookieParser());

    // gzip responses
    app.use(compression());

    // allow cors
    app.use(cors());

    app.use(
        session({
            store: new RedisStore({ client: redisClient }),
            secret: 'cat',
            resave: false,
            saveUninitialized: true,
        })
    );

    app.use(passport.initialize());
    app.use(passport.session());

    // add some headers
    app.use((req, res, next) => {
        res.locals.authUser = req.user;
        res.locals.authUserName = (req.user && (req.user.displayName || req.user.username)) || 'some cat';
        res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS ');
        res.header('Access-Control-Allow-Credentials', '*');
        res.header(
            'Access-Control-Allow-Headers',
            'Origin, X-Requested-With,'
            + ' Content-Type, Accept,'
            + ' Authorization,'
            + ' Access-Control-Allow-Credentials',
        );
        next();
    });
};
