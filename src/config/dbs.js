const mongoose = require('mongoose');

mongoose.set('useFindAndModify', false);

const MONGODB_URI = process.env.MONGO_URI || 'mongodb://localhost:27017/';
const MONGODB_DB_MAIN = process.env.DB_MAIN || 'users_db';
const MONGO_URI = `${MONGODB_URI}${MONGODB_DB_MAIN}`;

const connectOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};

module.exports = {
    main: mongoose.createConnection(MONGO_URI, connectOptions),
};
