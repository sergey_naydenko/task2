function handleErrors(error, req, res) {
    if (error instanceof APP.ValidationError) {
        return res.status(422).json({
            error: error.name,
            details: error.message,
        });
    }

    res.status(500).json({
        message: error.name,
        details: error.message,
    });
}

module.exports = (app) => {
    app.use(handleErrors);
};
