const { Schema, Types } = require('mongoose');

module.exports = APP.dbs.main.model('MessageModel', new Schema(
    {
        text: {
            type: String,
            trim: true,
            required: true,
        },
        name: {
            type: String,
            trim: true,
            default: 'anonymous',
        },
        topicId: {
            type: Types.ObjectId,
            required: true,
        },
    },
    {
        collection: 'messagemodel',
        versionKey: false,
        timestamps: true,
    },
));
