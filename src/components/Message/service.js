const Model = require('./model');
const TopicModel = require('../Topic/model');

function findAll(id) {
    return Model.find({ topicId: id }).exec();
}

function findById(id) {
    return Model.findById(id).exec();
}

async function create(profile) {
    const newMessage = await Model.create(profile);
    TopicModel.findByIdAndUpdate(newMessage.topicId, {
        $inc:
        {
            numberOfMessages: 1,
        }
    }).exec();
    return newMessage;
}

function updateById(_id, newProfile) {
    return Model.updateOne({ _id }, newProfile).exec();
}

async function deleteById(_id) {
    const currentMessage = await Model.findById(_id);
    TopicModel.findByIdAndUpdate(currentMessage.topicId, {
        $inc:
        {
            numberOfMessages: -1,
        }
    }).exec();
    return Model.deleteOne({ _id }).exec();
}
function getUniqUsersOfTopicMsg(dateStart, dateClose, topicId) {
    return Model.find({ topicId })
        .where('createdAt').gte(dateStart).lte(dateClose)
        .distinct('name')
        .exec();
}

module.exports = {
    findAll,
    findById,
    create,
    updateById,
    deleteById,
    getUniqUsersOfTopicMsg,
};
