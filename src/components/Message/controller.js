const Service = require('./service');
const TopicService = require('../Topic/service');
const Validation = require('./validation');

async function findAll(req, res) {
    const topic = await TopicService.findById(req.params.topicId);

    if (!topic) return res.redirect('/');

    const messages = await Service.findAll(req.params.topicId);

    res.render('message/findAll', {
        title: 'all strings',
        messages,
        topic,
    });
}

async function findById(req, res) {
    const { error } = Validation.findById(req.params);

    if (error) {
        throw new APP.ValidationError(error.details);
    }

    const stringDoc = await Service.findById(req.params.id);

    return res.render('index', { title: stringDoc.text, message: stringDoc.text });
}

async function create(req, res) {
    const { error } = Validation.create(req.body);

    if (error) {
        throw new APP.ValidationError(error.details);
    }

    await Service.create(req.body);
    await TopicService.updateById(req.body.topicId, {
        updatedAt: new Date()
    });

    return res.redirect(`/v1/topic/${req.body.topicId}/message`);
}

async function updateById(req, res) {
    const { error } = Validation.updateById(req.body);

    if (error) {
        throw new APP.ValidationError(error.details);
    }

    const updatedUser = await Service.updateById(req.body.id, req.body);

    return res.status(200).json({
        data: updatedUser,
    });
}

async function deleteById(req, res) {
    const { error } = Validation.deleteById(req.body);

    if (error) {
        throw new APP.ValidationError(error.details);
    }

    const deletedUser = await Service.deleteById(req.body.id);

    return res.status(200).json({
        data: deletedUser,
    });
}

async function getUniqUsersOfTopicMsg(req, res) {
    const { error } = Validation.getUniqUsersOfTopicMsg(req.body);

    if (error) {
        throw new APP.ValidationError(error.details);
    }
    const dateStart = new Date(req.body.dateStart);
    const dateClose = new Date(req.body.dateClose);

    const users = await Service.getUniqUsersOfTopicMsg(dateStart, dateClose, req.params.topicId);
    return res.status(200).json({
        data: users,
    });
}

module.exports = {
    findAll,
    findById,
    create,
    updateById,
    deleteById,
    getUniqUsersOfTopicMsg,
};
