const { Router } = require('express');
const Actions = require('./controller');

const router = Router({
    mergeParams: true,
});

router.get('/user-names', APP.asyncWrapper(Actions.getUniqUsersOfTopicMsg));
router.get('/', APP.asyncWrapper(Actions.findAll));
router.get('/:id', APP.asyncWrapper(Actions.findById));
router.post('/', APP.asyncWrapper(Actions.create));
router.put('/', APP.asyncWrapper(Actions.updateById));
router.delete('/', APP.asyncWrapper(Actions.deleteById));

module.exports = router;
