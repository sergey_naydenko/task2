const Validation = require('../../config/globals/validation');

/**
 * @exports
 * @class
 * @extends Validation
 */
class StringValidation extends Validation {
    /**
     * @param {String} data.id - objectId
     * @returns
     * @memberof StringValidation
     */
    findById(data) {
        return this.Joi
            .object({
                id: this.Joi.objectId(),
            })
            .validate(data);
    }

    /**
     * @param {String} profile.email
     * @param {String} profile.fullName
     * @returns
     * @memberof StringValidation
     */
    create(profile) {
        return this.Joi
            .object({
                text: this.Joi.string()
                    .min(1)
                    .max(4096)
                    .required(),
                name: this.Joi.string()
                    .allow('')
                    .min(1)
                    .max(256),
                topicId: this.Joi.objectId()
                    .required(),
            })
            .validate(profile);
    }

    /**
     * @param {String} data.id - objectId
     * @param {String} data.fullName
     * @returns
     * @memberof StringValidation
     */
    updateById(data) {
        return this.Joi
            .object({
                id: this.Joi.objectId(),
                fullName: this.Joi
                    .string()
                    .min(1)
                    .max(30)
                    .required(),
            })
            .validate(data);
    }

    /**
     * @param {String} data.id - objectId
     * @returns
     * @memberof StringValidation
     */
    // eslint-disable-next-line sonarjs/no-identical-functions
    deleteById(data) {
        return this.Joi
            .object({
                id: this.Joi.objectId(),
            })
            .validate(data);
    }

    /**
     *
     * @param data.dateStart & data.dateClose
     * @returns {*}
     */
    getUniqUsersOfTopicMsg(data) {
        return this.Joi
            .object({
                dateStart: this.Joi.date(),
                dateClose: this.Joi.date(),
            })
            .validate(data);
    }
}

module.exports = new StringValidation();
