async function loginPage(req, res) {
    res.render('authorization/login');
}

async function login(req, res) {
    res.redirect('/v1/topic');
}

async function logout(req, res) {
    req.logout();
    req.session.destroy(() => {
        res.redirect('/v1/topic');
    });
}

module.exports = {
    loginPage,
    login,
    logout,
};
