const { Router } = require('express');

const Controller = require('./controller');
require('./passportSetup');
const strategies = require('./strategies');

const router = Router();

strategies.init(router);

router.get('/login', APP.asyncWrapper(Controller.loginPage));

router.get('/logout', APP.asyncWrapper(Controller.logout));

module.exports = router;
