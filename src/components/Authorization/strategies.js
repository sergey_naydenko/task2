/* eslint-disable global-require */
const passport = require('passport');

const FacebookStrategy = require('passport-facebook').Strategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const GitHubStrategy = require('passport-github2').Strategy;

let router;

function createOauth2Strategy([name, Strategy, scope]) {
    const NAME = name.toUpperCase();
    const clientIDEnvName = `${NAME}_CLIENT_ID`;
    // eslint-disable-next-line security/detect-object-injection
    const clientID = process.env[clientIDEnvName];
    const clientSecretEnvName = `${NAME}_CLIENT_SECRET`;
    // eslint-disable-next-line security/detect-object-injection
    const clientSecret = process.env[clientSecretEnvName];

    if (!clientID) {
        console.warn(`clientID missing for ${name} auth strategy. please set ${clientIDEnvName}`);
        return;
    }
    if (!clientSecret) {
        console.warn(`clientSecret missing for ${name} auth strategy. please set ${clientSecretEnvName}`);
        return;
    }

    passport.use(new Strategy(
        {
            clientID,
            clientSecret,
            callbackURL: `${APP.baseUrl}v1/auth/${name}/callback`
        },
        (accessToken, refreshToken, profile, done) => done(null, profile)
    ));

    router.get(`/${name}`, passport.authenticate(name, { ...scope && { scope } }));
    router.get(`/${name}/callback`, passport.authenticate(name, { failureRedirect: '/v1/auth/login', successRedirect: '/' }));
}

function init(_router) {
    router = _router;

    [
        ['facebook', FacebookStrategy],
        ['google', GoogleStrategy, ['profile', 'email']],
        ['github', GitHubStrategy, ['user:email']],
    ].forEach(createOauth2Strategy);
}

module.exports = {
    init
};
