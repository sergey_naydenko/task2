const Service = require('./service');
const Validation = require('./validation');

Service.numberOfMessages();

async function findAll(req, res) {
    const [topics, pinned, recent] = await Promise.all([
        Service.findAll(),
        Service.pinned(),
        Service.recent(),
    ]);
    res.render('topic/findAll', {
        topics,
        recent,
        pinned,
        title: 'all strings',
    });
}

async function findById(req, res) {
    const { error } = Validation.findById(req.params);

    if (error) {
        throw new APP.ValidationError(error.details);
    }

    const topic = await Service.findById(req.params.id);

    // return res.render('index', { title: stringDoc.text, message: stringDoc.text });
    return res.status(200).json({
        topic,
    });
}

async function pinTopic(req, res) {
    await Service.pinTopic(req.params.topicId);
    return res.redirect(`/v1/topic/${req.params.topicId}/message`);
}

async function unpinTopic(req, res) {
    await Service.unpinTopic(req.params.topicId);
    return res.redirect(`/v1/topic/${req.params.topicId}/message`);
}

async function create(req, res) {
    if (req.body.pinned === 'on') {
        req.body.pinned = true;
    }

    const { error } = Validation.create(req.body);
    if (error) {
        throw new APP.ValidationError(error.details);
    }

    const newTopic = await Service.create(req.body);

    return res.redirect(`/v1/topic/${newTopic.id}/message`);
}

/**
 * @function
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {express.NextFunction} next
 * @returns {Promise<void>}
 */
async function updateById(req, res) {
    const { error } = Validation.updateById(req.body);

    if (error) {
        throw new APP.ValidationError(error.details);
    }

    const updatedUser = await Service.updateById(req.body.id, req.body);

    return res.status(200).json({
        data: updatedUser,
    });
}

async function deleteById(req, res) {
    const { error } = Validation.deleteById(req.body);

    if (error) {
        throw new APP.ValidationError(error.details);
    }

    const deletedUser = await Service.deleteById(req.body.id);

    return res.status(200).json({
        data: deletedUser,
    });
}

async function getUniqUsers(req, res) {
    const { error } = Validation.getUniqUsers(req.query);

    if (error) {
        throw new APP.ValidationError(error.details);
    }

    const dateStart = new Date(req.query.dateStart || APP.moment().subtract(7, 'days'));
    const dateClose = new Date(req.query.dateClose || APP.moment().endOf('day'));

    const users = await Service.getUniqUsers(dateStart, dateClose);
    return res.status(200).json({
        data: users,
    });
}

module.exports = {
    findAll,
    findById,
    create,
    updateById,
    deleteById,
    getUniqUsers,
    pinTopic,
    unpinTopic,
};
