/**
 * @exports
 * @class
 * @extends Validation
 */
const AUTHOR_NAME_MIN_LENGTH = 1;
const AUTHOR_NAME_MAX_LENGTH = 50;
const TITLE_MIN_LENGTH = 1;
const TITLE_MAX_LENGTH = 256;
const DESCRIPTION_MAX_LENGTH = 4096;

class TopicValidation extends APP.Validation {
    /**
     * @param {String} data.id - objectId
     * @returns
     * @memberof TopicValidation
     */
    findById(data) {
        return this.Joi
            .object({
                id: this.Joi.objectId(),
            })
            .validate(data);
    }

    /**
     * @param {String} profile.email
     * @param {String} profile.fullName
     * @returns
     * @memberof TopicValidation
     */
    create(profile) {
        return this.Joi
            .object({
                author: this.Joi.string()
                    .min(AUTHOR_NAME_MIN_LENGTH)
                    .max(AUTHOR_NAME_MAX_LENGTH)
                    .required(),
                title: this.Joi.string()
                    .min(TITLE_MIN_LENGTH)
                    .max(TITLE_MAX_LENGTH)
                    .required(),
                description: this.Joi.string()
                    .min(TITLE_MIN_LENGTH)
                    .max(DESCRIPTION_MAX_LENGTH),
                pinned: this.Joi.boolean(),
            })
            .validate(profile);
    }

    /**
     * @param {String} data.id - objectId
     * @param {String} data.fullName
     * @returns
     * @memberof TopicValidation
     */
    updateById(data) {
        return this.Joi
            .object({
                id: this.Joi.objectId(),
                title: this.Joi
                    .string()
                    .min(TITLE_MIN_LENGTH)
                    .max(TITLE_MAX_LENGTH)
                    .required(),
            })
            .validate(data);
    }

    /**
     * @param {String} data.id - objectId
     * @returns
     * @memberof TopicValidation
     */
    // eslint-disable-next-line sonarjs/no-identical-functions
    deleteById(data) {
        return this.Joi
            .object({
                id: this.Joi.objectId(),
            })
            .validate(data);
    }

    /**
     * @param { Date } data.dateStart
     * @param { Date } data.dateClose 1
     * @returns {*}
     */
    getUniqUsers(data) {
        return this.Joi
            .object({
                dateStart: this.Joi.date(),
                dateClose: this.Joi.date(),
            })
            .validate(data);
    }
}

module.exports = new TopicValidation();
