const { Schema } = require('mongoose');

module.exports = APP.dbs.main.model('TopicModel', new Schema(
    {
        author: {
            type: String,
            trim: true,
            required: true,
        },
        title: {
            type: String,
            trim: true,
            required: true,
        },
        description: {
            type: String,
            trim: true,
            required: false,
        },
        pinned: {
            type: Boolean,
            required: false,
            default: false,
        },
        numberOfMessages: {
            type: Number,
            required: false,
            default: 0,
        },
    },
    {
        collection: 'topicmodel',
        versionKey: false,
        timestamps: true,
    },
));
