const Model = require('./model');
const MessageModel = require('../Message/model');

function findAll() {
    // Sorting topics in descending order.
    return Model.find({}).sort({ createdAt: -1 }).exec();
}
function recent() {
    /* sort by updateAt and get first */
    return Model.findOne({}).sort({ updatedAt: -1 }).exec();
}

async function pinned() {
    const topic = await Model.findOne({ pinned: 'true' }).exec();
    return await topic === null
        ? Model.findOne({}).exec()
        : topic;
}

async function pinTopic(topicId) {
    if (await Model.findOne({ pinned: true }).exec() === null) {
        await Model.findOneAndUpdate({ pinned: true },
            {
                $set: {
                    pinned: false
                }
            }).exec();
        await Model.findByIdAndUpdate(topicId,
            {
                $set: {
                    pinned: true
                }
            }).exec();
    }
}

async function unpinTopic(topicId) {
    const currentTopic = await Model.findById(topicId).exec();
    if (currentTopic.pinned === true) {
        await Model.findByIdAndUpdate(topicId,
            {
                $set: {
                    pinned: false
                }
            }).exec();
    }
}

function findById(id) {
    return Model.findById(id).exec();
}

function getUniqUsers(dateStart, dateClose) {
    return Model.find({ author: { $ne: null } })
        .where('createdAt').gt(dateStart).lt(dateClose)
        .distinct('author')
        .exec();
}

/**
 * @exports
 * @method create
 * @param {object} profile
 * @summary create a new user
 * @returns {Promise<UserModel>}
 */
async function create(profile) {
    if (profile.pinned) {
        await Model.findOneAndUpdate({ pinned: true },
            {
                $set: {
                    pinned: false
                }
            }).exec();
        profile.pinned = true;
    }
    return Model.create(profile);
}

function updateById(_id, fieldsToUpdate) {
    return Model.updateOne({ _id }, fieldsToUpdate).exec();
}

/**
 * @exports
 * @method deleteById
 * @param {string} _id
 * @summary delete a user from database
 * @returns {Promise<void>}
 */
function deleteById(_id) {
    return Model.deleteOne({ _id }).exec();
}

async function numberOfMessages() {
    const countMessagesByTopic = await MessageModel.aggregate([
        {
            $group: {
                _id: '$topicId',
                count: { $sum: 1 }
            }
        }
    ]).exec();

    await countMessagesByTopic.forEach(element => {
        Model.findByIdAndUpdate(element.id, { $set: { numberOfMessages: element.count } }).exec();
    });
}

module.exports = {
    findAll,
    recent,
    pinned,
    pinTopic,
    unpinTopic,
    findById,
    create,
    updateById,
    deleteById,
    numberOfMessages,
    getUniqUsers,
};
