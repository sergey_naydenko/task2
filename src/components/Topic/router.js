const { Router } = require('express');
const Controller = require('./controller');

const router = Router();

router.get('/user-names', APP.asyncWrapper(Controller.getUniqUsers));
router.get('/', APP.asyncWrapper(Controller.findAll));
router.get('/:id', APP.asyncWrapper(Controller.findById));
router.post('/', APP.asyncWrapper(Controller.create));
router.put('/', APP.asyncWrapper(Controller.updateById));
router.get('/:topicId/pin', APP.asyncWrapper(Controller.pinTopic));
router.get('/:topicId/unpin', APP.asyncWrapper(Controller.unpinTopic));
router.delete('/', APP.asyncWrapper(Controller.deleteById));

module.exports = router;
