# Blog

Simple Blog create with Node.js fast-progress REST API with Express and Mongoose. It's based on yoeman generator - [node-express-fast-progress](https://github.com/ChechaValerii/node-express-fast-progress)


![CircleCI branch](https://img.shields.io/circleci/project/github/RedSparr0w/node-csgo-parser/master.svg?style=flat-square)
![onix](https://img.shields.io/badge/onix-systems-blue.svg)
![create with](https://img.shields.io/badge/create%20with-%F0%9F%92%96-05B38E)

![preview](https://i.ibb.co/gJQr7cr/Screen-Capture-select-area-20201203173824.png)

## Description

We are interns and this is our join project - Blog on which we work during our internship at [Onix-Systems](https://onix-systems.com/).

## Routers

### Topic component

find all topics
```bash
    curl --location --request GET 'localhost:3000/v1/topic/'
    --header 'Content-Type: application/json'
```

find topic by id
```bash
    curl --location --request GET 'localhost:3000/v1/topic/<<id>>/'
    --header 'Content-Type: application/json'
```

create topic
```bash
    curl --location --request POST 'localhost:3000/v1/topic/'
    --header 'Content-Type: application/json'
        --data-raw '{
        "author": "<<author>>",
        "title": "<<title>>",
        "description": "<<description>>"
    }'
```

update topic by id
```bash
    curl --location --request PUT 'localhost:3000/v1/topic/'
    --header 'Content-Type: application/json'
        --data-raw '{
        "id": "<<id>>",
        "title": "<<title>>"
    }'
```

delete topic by id
```bash
    curl --location --request DELETE 'localhost:3000/v1/topic/'
    --header 'Content-Type: application/json'
        --data-raw '{
        "id": "<<id>>",
    }'
```

### Message component

find all messages
```bash
    curl --location --request GET 'localhost:3000/v1/topic/<<topicId>>/message/'
    --header 'Content-Type: application/json'
```

find message by id
```bash
    curl --location --request GET 'localhost:3000/v1/topic/<<topicId>>/message/<<id>>/'
    --header 'Content-Type: application/json'
```

create message
```bash
    curl --location --request POST 'localhost:3000/v1/topic/<<topicId>>/message/'
    --header 'Content-Type: application/json'
        --data-raw '{
        "text": "<<text>>",
        "name": "<<name>>",
        "topicId": "<<topicId>>"
    }'
```

update message by id
```bash
    curl --location --request PUT 'localhost:3000/v1/topic/<<topicId>>/message/'
    --header 'Content-Type: application/json'
        --data-raw '{
        "id": "<<id>>",
        "fullName": "<<fullName>>"
    }'
```

delete message by id
```bash
    curl --location --request DELETE 'localhost:3000/v1/topic/<<topicId>>/message/'
    --header 'Content-Type: application/json'
        --data-raw '{
        "id": "<<id>>",
    }'
```

### Project Introduction
- suppot ES6/ES7 features
- using eslint followed [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript)

## Requirements
- [Node](https://nodejs.org/)) >= 14
- [npm](https://www.npmjs.com/) >= 6
- [MongoDB](https://www.mongodb.com/try/download/community) >= 4.0

## Installation
First, clone this repo. Then configure required connections to Redis and mongoDb.

Install dependencies
```bash
npm i
```

## Running the API

### Development
To start the application in development mode, run:

```bash
npm i -g nodemon
npm i
```

Start the application in dev env:
```
nodemon
```
Start the application in production env:

Install PM2:
```
npm i -g pm2
```

example start with scale on 2 core:
```
pm2 start ./src/index.js -i 2 --no-daemon
```

Express server listening on http://localhost:3000/, in development mode. The developer mode will watch your changes and re-run the node application automatically.